package com.job.finder.db.search;

import com.job.finder.db.entities.JobEntity;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JobSearch implements Specification<JobEntity> {
    @Singular(ignoreNullCollections = true)
    private List<String> jobFunctions;
    @Singular(ignoreNullCollections = true)
    private List<String> objectIDs;

    @Override
    public Predicate toPredicate(Root<JobEntity> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        ArrayList<Predicate> predicates = new ArrayList<>();

        if (!CollectionUtils.isEmpty(jobFunctions)){
            predicates.add(root.get("jobFunction").in(jobFunctions));
        }
        if (!CollectionUtils.isEmpty(objectIDs)){
            predicates.add(root.get("objectID").in(objectIDs));
        }

        return cb.and(predicates.toArray(new Predicate[0]));
    }
}
