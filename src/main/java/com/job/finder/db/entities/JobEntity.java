package com.job.finder.db.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@Entity
@Table(name = "job")
@NoArgsConstructor
@AllArgsConstructor
public class JobEntity {
    @Id
    @Column(name = "objectID")
    private String objectID;
    @Column(name = "createdAt")
    private Timestamp createdAt;
    private String location;
    @Column(name = "positionName")
    private String positionName;
    @Column(name = "organizationName")
    private String organizationName;
    @Column(name = "organizationLogo")
    private String organizationLogo;
    private String url;
    private String tags;
    private String description;
    @Column(name = "jobFunction")
    private String jobFunction;
}
