package com.job.finder.web.dtos;


public record SaveArchiveJob(
        String id
) {
}
