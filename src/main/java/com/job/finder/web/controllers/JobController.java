package com.job.finder.web.controllers;

import com.job.finder.domain.filters.JobFilter;
import com.job.finder.domain.models.ConverterType;
import com.job.finder.domain.models.Job;
import com.job.finder.domain.models.JobConverterHolder;
import com.job.finder.domain.services.JobService;
import com.job.finder.web.dtos.SaveArchiveJob;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.thymeleaf.util.ArrayUtils;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Controller
public class JobController {
    private final List<String> jobFunctions = List.of(
            "Accounting & Finance",
            "Administration",
            "Customer Service",
            "Data Science",
            "Design",
            "IT",
            "Legal",
            "Marketing & Communications",
            "Operations",
            "Other Engineering",
            "People & HR",
            "Product");
    private final JobService jobService;

    @GetMapping({"/", "/jobs"})
    public String getJobs(Model model, JobFilter filter) {
        model.addAttribute("container", jobService.findBy(filter));
        model.addAttribute("filter", filter);

        Set<String> jobFunctionFilter;

        if (ArrayUtils.isEmpty(filter.getJobFunctions())) {
            jobFunctionFilter = Set.of();
        } else {
            jobFunctionFilter = Arrays.stream(filter.getJobFunctions()).collect(Collectors.toSet());
        }

        model.addAttribute("jobFunctionMap", jobFunctions
                .stream()
                .collect(Collectors.toMap(it -> it, jobFunctionFilter::contains, (a, b) -> b, LinkedHashMap::new)));

        return "JobsPage";
    }

    @GetMapping("/jobs/{id}")
    public String getJob(Model model, @PathVariable String id) {
        var job = jobService.findById(id);

        if (job.isEmpty()) {
            return "redirect:/jobs";
        }

        model.addAttribute("job", job.get());
        model.addAttribute("isArchive", jobService.isArchived(job.get().getObjectID()));

        return "JobPage";
    }

    @PostMapping("/jobs")
    public String saveJob(Model model, SaveArchiveJob job) {

        var optJob = jobService.findById(job.id());

        optJob.ifPresent(jobService::save);

        return optJob.map(value -> "redirect:/jobs/" + value.getObjectID()).orElse("redirect:/jobs");

    }

    @DeleteMapping("/jobs/{id}")
    public String deleteJob(@PathVariable String id) {
        jobService.delete(id);
        return "redirect:/jobs/" + id;
    }

}
