package com.job.finder.web.controllers;

import com.job.finder.domain.filters.JobFilter;
import com.job.finder.domain.models.ConverterType;
import com.job.finder.domain.models.JobConverterHolder;
import com.job.finder.domain.services.JobService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
@RequiredArgsConstructor
public class FileJobController {
    private final JobService jobService;
    private final JobConverterHolder jobConverterHolder;

    @GetMapping("/jobs/convert/{type}")
    public ResponseEntity<byte[]> convert(@PathVariable ConverterType type, JobFilter filter){
        if (type == null){
            return ResponseEntity.badRequest().build();
        }

        var jobs = jobService.findBy(filter).getContent();

        var bytes = jobConverterHolder.getConverter(type).map(converter -> converter.convert(jobs)).orElse(new byte[0]);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("attachment", "file_result." + type.getExtension());
        headers.setContentLength(bytes.length);

        return ResponseEntity.ok().headers(headers).body(bytes);
    }
}
