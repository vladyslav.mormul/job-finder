package com.job.finder.net.clients;

import com.job.finder.net.mappers.HitSearchUriMapper;
import com.job.finder.net.responses.JobDescribeResponse;
import com.job.finder.net.responses.JobResponse;
import com.job.finder.net.responses.ResultResponse;
import com.job.finder.net.search.HitSearch;
import lombok.RequiredArgsConstructor;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.http.HttpMethod;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@RequiredArgsConstructor
public class RestHitClient implements HitClient{
    private final HitSearchUriMapper hitSearchUriMapper;
    private final URI uri = URI.create("https://jobs.techstars.com/api/search/jobs?networkId=89");
    private final URI simpleUri = URI.create("https://jobs.techstars.com");

    private final RestTemplate restTemplate = new RestTemplate();
    @Override
    public ResultResponse findBy(HitSearch search) {
        var url = hitSearchUriMapper.mapToUrl(uri, search);

        return restTemplate.exchange(url, HttpMethod.GET, null, ResultResponse.class).getBody();
    }

    @Override
    public JobDescribeResponse getDescription(String id) {
        JobDescribeResponse response = new JobDescribeResponse();
        response.setDescription("NOT_FOUND");

        try {
            response.setJob(
                    findBy(HitSearch.builder().objectID(id).build()).getResults()[0].getJobs()[0]
            );
        }catch (Exception ignored){}

        if (response.getJob() == null) return response;

        if (!response.getJob().getHasDescription()) return response;

        try {
            Document document =Jsoup.connect(toPageUrl(response.getJob())).get();

            var text = document.select("[data-testid=careerPage]").html();
            if (StringUtils.hasText(text)){
                response.setDescription(text);
            }

        }catch (Exception ignored){}

        return response;
    }

    private String toPageUrl(JobResponse response){
        UriBuilder builder = UriComponentsBuilder.fromUri(simpleUri);

        builder.pathSegment("companies", response.getOrganization().getSlug(), "jobs", response.getSlug());

        return builder.build().toString();
    }
}
