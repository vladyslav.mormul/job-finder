package com.job.finder.net.clients;

import com.job.finder.net.responses.JobDescribeResponse;
import com.job.finder.net.responses.ResultResponse;
import com.job.finder.net.search.HitSearch;

public interface HitClient {
    ResultResponse findBy(HitSearch search);

    JobDescribeResponse getDescription(String id);

}
