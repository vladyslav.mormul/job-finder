package com.job.finder.net.mappers;

import com.job.finder.net.search.HitSearch;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.util.UriBuilder;
import org.springframework.web.util.UriComponentsBuilder;
import org.yaml.snakeyaml.util.UriEncoder;

import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class HitSearchUriMapper {


    public URI mapToUrl(URI uri, HitSearch search) {
        UriBuilder uriBuilder = UriComponentsBuilder.fromUri(uri);

        long page = Objects.requireNonNullElse(search.getPage(), 0L);
        long size = Objects.requireNonNullElse(search.getSize(), 10L);

        if (page < 0) {
            page = 0L;
        }
        if (size < 1) {
            size = 1L;
        }

        uriBuilder.queryParam("page", page);
        uriBuilder.queryParam("hitsPerPage", size);

        var queryBuilder = new ArrayList<String>();

        if (!CollectionUtils.isEmpty(search.getJobFunctions())) {
            queryBuilder.add(toQuery("job_functions",search.getJobFunctions()));
        }

        if (!CollectionUtils.isEmpty(search.getObjectIDs())) {
            queryBuilder.add(toQuery("objectID",search.getObjectIDs()));
        }

        uriBuilder.queryParam("filters", String.join(" AND ", queryBuilder));


        return uriBuilder.build();
    }

    private String toQuery(String name, Collection<String> params){
        return  "(" +
                params
                        .stream()
                        .map((it) -> name + ":\"" + it + "\"")
                        .collect(Collectors.joining(" OR "))
                + ")";
    }
}
