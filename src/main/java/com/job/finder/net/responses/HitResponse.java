package com.job.finder.net.responses;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HitResponse {
    @JsonProperty("hits")
    private JobResponse[] jobs;
    private Long page;
    @JsonProperty("nbPages")
    private Long totalPages;
    @JsonProperty("hitsPerPage")
    private Long size;
    @JsonProperty("nbHits")
    private Long totalElements;
}
