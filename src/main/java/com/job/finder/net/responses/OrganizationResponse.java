package com.job.finder.net.responses;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrganizationResponse {
    @JsonProperty("logo_url")
    private String logoUrl;
    private String name;
    private String slug;
    @JsonProperty("industry_tags")
    private String[] tags;
}
