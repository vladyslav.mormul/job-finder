package com.job.finder.net.responses;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JobResponse {
    private String objectID;
    @JsonProperty("created_at")
    private Long createdAt;
    private String[] locations;
    private String slug;
    private String url;
    @JsonProperty("title")
    private String positionName;
    @JsonProperty("has_description")
    private Boolean hasDescription;
    private OrganizationResponse organization;
    @JsonProperty("_highlightResult")
    private HighlightResultResponse highlightResult;


}
