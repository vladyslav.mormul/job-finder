package com.job.finder.net.responses;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HighlightResultResponse {
    @JsonProperty("job_functions")
    private Item[] jobFunctions;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Item {
        private String value;

        public String toString() {
            return value;
        }
    }
}
