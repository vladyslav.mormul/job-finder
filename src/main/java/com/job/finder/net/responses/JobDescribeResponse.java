package com.job.finder.net.responses;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JobDescribeResponse {
    private JobResponse job;
    private String description;
}
