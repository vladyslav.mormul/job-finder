package com.job.finder.net.search;

import lombok.*;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HitSearch {
    private Long page;
    private Long size;
    @Singular(ignoreNullCollections = true)
    private List<String> jobFunctions;
    @Singular(ignoreNullCollections = true)
    private List<String> objectIDs;
}
