package com.job.finder.net;

import com.job.finder.net.clients.HitClient;
import com.job.finder.net.clients.RestHitClient;
import com.job.finder.net.mappers.HitSearchUriMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class NetClientConfig {

    @Bean
    public HitClient restHitClient(HitSearchUriMapper uriMapper) {
        return new RestHitClient(uriMapper);
    }
}
