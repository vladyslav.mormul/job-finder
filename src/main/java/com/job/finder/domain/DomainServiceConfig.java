package com.job.finder.domain;

import com.job.finder.db.repositories.JobRepository;
import com.job.finder.domain.mappers.*;
import com.job.finder.domain.services.JobService;
import com.job.finder.domain.services.JobServiceImpl;
import com.job.finder.net.clients.HitClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DomainServiceConfig {

    @Bean
    public JobService jobServiceImpl(
            HitClient hitClient,
            JobFilterMapper jobFilterMapper,
            ResultResponseMapper resultResponseMapper,
            JobMapper jobMapper,
            PagesMapper pagesMapper,
            JobRepository jobRepository,
            JobDbSearchMapper jobDbSearchMapper,
            JobEntityMapper jobEntityMapper
    ) {
        return new JobServiceImpl(
                hitClient, jobFilterMapper, resultResponseMapper,
                jobMapper, pagesMapper, jobRepository,jobDbSearchMapper,
                jobEntityMapper
        );
    }
}
