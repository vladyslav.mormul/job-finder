package com.job.finder.domain.services;

import com.job.finder.db.repositories.JobRepository;
import com.job.finder.db.search.JobSearch;
import com.job.finder.domain.filters.JobFilter;
import com.job.finder.domain.mappers.*;
import com.job.finder.domain.models.ContentContainer;
import com.job.finder.domain.models.Job;
import com.job.finder.net.clients.HitClient;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

@RequiredArgsConstructor
public class JobServiceImpl implements JobService {
    private final HitClient hitClient;
    private final JobFilterMapper jobFilterMapper;
    private final ResultResponseMapper resultResponseMapper;
    private final JobMapper jobMapper;
    private final PagesMapper pagesMapper;
    private final JobRepository jobRepository;
    private final JobDbSearchMapper jobDbSearchMapper;
    private final JobEntityMapper jobEntityMapper;

    @Override
    public ContentContainer<Job> findBy(JobFilter filter) {
        if (filter.isArchive()){
            var searchEntry = jobDbSearchMapper.mapToJobSearch(filter);

            var pages = jobRepository.findAll(
                    searchEntry.getKey(),
                    searchEntry.getValue()
            );
            return pagesMapper.toContentContainer(pages);
        }

        return resultResponseMapper.toContentContainer(
                hitClient.findBy(jobFilterMapper.toHitSearch(filter))
        );
    }

    @Override
    public Optional<Job> findById(String id) {
        var res = hitClient.getDescription(id);

        if (res.getJob() == null) return Optional.empty();

        var job = jobMapper.toJob(res.getJob());

        job.setDescription(res.getDescription());

        return Optional.of(job);
    }

    @Override
    public boolean save(Job job) {
        try {
            jobRepository.save(jobEntityMapper.toJobEntity(job));
            return true;
        }catch (RuntimeException e){
            return false;
        }
    }

    @Override
    @Transactional
    public boolean delete(String id) {
        return jobRepository.delete(JobSearch.builder().objectID(id).build()) != 0;
    }

    @Override
    public boolean isArchived(String id) {
        return jobRepository.exists(JobSearch.builder().objectID(id).build());
    }
}
