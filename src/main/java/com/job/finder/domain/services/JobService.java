package com.job.finder.domain.services;

import com.job.finder.domain.filters.JobFilter;
import com.job.finder.domain.models.ContentContainer;
import com.job.finder.domain.models.Job;

import java.util.Optional;

public interface JobService {

    ContentContainer<Job> findBy(JobFilter filter);

    Optional<Job> findById(String id);

    boolean save(Job job);

    boolean delete(String id);

    boolean isArchived(String id);
}
