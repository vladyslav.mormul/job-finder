package com.job.finder.domain.converters;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.job.finder.domain.models.ConverterType;
import com.job.finder.domain.models.Job;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SqlJobConverter implements JobConverter {
    private final static String CREATE_DB = """
            drop database IF exists job_db;
            CREATE DATABASE job_db;
            use job_db;

            """;
    private final static String CREATE_TABLE = """
    CREATE TABLE job (
        objectID VARCHAR(255) PRIMARY KEY,
        createdAt TIMESTAMP,
        location VARCHAR(255),
        positionName VARCHAR(255),
        organizationName VARCHAR(255),
        organizationLogo VARCHAR(255),
        url VARCHAR(255),
        description TEXT,
        jobFunction VARCHAR(255),
        tags JSON
    );
    """;
    private final static JsonMapper JSON_MAPPER = new JsonMapper();


    public String generateInsertSQL(Job job) throws JsonProcessingException {
        return "INSERT INTO job (objectID, createdAt, location, positionName, organizationName, organizationLogo, url, description, jobFunction,tags) VALUES ('"
                + job.getObjectID() + "', '"
                + job.getCreatedAt() + "', '"
                + job.getLocation() + "', '"
                + job.getPositionName() + "', '"
                + job.getOrganizationName() + "', '"
                + job.getOrganizationLogo() + "', '"
                + job.getUrl() + "', '"
                + job.getDescription() + "', '"
                + job.getJobFunction() + "', '"
                + JSON_MAPPER.writeValueAsString(job.getTags()) + "');";
    }

    @Override
    public ConverterType getType() {
        return ConverterType.SQL;
    }

    @Override
    public byte[] convert(List<Job> jobs) {
        StringBuilder sb = new StringBuilder();

        sb.append(CREATE_DB);
        sb.append(CREATE_TABLE);
        jobs.forEach(job -> {
            try {
                sb.append(generateInsertSQL(job)).append("\n");
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        });

        return sb.toString().getBytes();
    }
}
