package com.job.finder.domain.converters;

import com.job.finder.domain.models.ConverterType;
import com.job.finder.domain.models.Job;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import org.springframework.stereotype.Component;

import java.io.StringWriter;
import java.util.List;

@Component
public class CsvJobConverter implements JobConverter{
    private final String[] headers = new String[]{
            "objectID",
            "createdAt",
            "location",
            "positionName",
            "organizationName",
            "organizationLogo",
            "url",
            "jobFunction",
            "description",
            "tags"
    };
    @Override
    public ConverterType getType() {
        return ConverterType.CSV;
    }

    @Override
    public byte[]  convert(List<Job> jobs) {
        StringWriter stringWriter = new StringWriter();

        try (CSVWriter writer = new CSVWriter(stringWriter)) {
            writer.writeNext(headers);
            for (Job job : jobs) {
                writer.writeNext(toRow(job));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return stringWriter.toString().getBytes();
    }

    private String[] toRow(Job job){
        return new String[]{
                job.getObjectID(),
                job.getCreatedAt().toString(),
                job.getLocation(),
                job.getPositionName(),
                job.getOrganizationName(),
                job.getOrganizationLogo(),
                job.getUrl(),
                job.getJobFunction(),
                job.getDescription(),
                job.getTags().toString()
        };
    }
}
