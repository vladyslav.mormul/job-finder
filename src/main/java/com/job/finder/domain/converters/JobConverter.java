package com.job.finder.domain.converters;

import com.job.finder.domain.models.ConverterType;
import com.job.finder.domain.models.Job;

import java.util.List;

public interface JobConverter {
    ConverterType getType();

    byte[] convert(List<Job> jobs);
}
