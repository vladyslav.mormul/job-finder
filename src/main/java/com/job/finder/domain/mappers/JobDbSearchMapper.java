package com.job.finder.domain.mappers;

import com.job.finder.db.search.JobSearch;
import com.job.finder.domain.filters.JobFilter;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.thymeleaf.util.ArrayUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

@Component
public class JobDbSearchMapper {

    public Map.Entry<JobSearch, Pageable>  mapToJobSearch(JobFilter filter) {
        JobSearch jobSearch = new JobSearch();

        if (ArrayUtils.isEmpty(filter.getJobFunctions())){
            jobSearch.setJobFunctions(Collections.emptyList());
        }else {
            jobSearch.setJobFunctions(Arrays.stream(filter.getJobFunctions()).toList());
        }

        if (ArrayUtils.isEmpty(filter.getObjectIDs())){
            jobSearch.setObjectIDs(Collections.emptyList());
        }else {
            jobSearch.setObjectIDs(Arrays.stream(filter.getObjectIDs()).toList());
        }

        int page = 0;
        int size = 10;

        if (filter.getPage() != null) {
            page = filter.getPage().intValue();
        }

        if (filter.getSize() != null) {
            size = filter.getSize().intValue();
        }


        return Map.entry(jobSearch, PageRequest.of(page, size));
    }
}
