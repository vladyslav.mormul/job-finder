package com.job.finder.domain.mappers;

import com.job.finder.domain.models.Job;
import com.job.finder.net.responses.HighlightResultResponse;
import com.job.finder.net.responses.JobResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriBuilder;
import org.springframework.web.util.UriComponentsBuilder;
import org.thymeleaf.util.ArrayUtils;

import java.net.URI;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;

@Component
public class JobMapper {
    private final URI uri = URI.create("https://jobs.techstars.com");

    public Job toJob(JobResponse response){
        Job job = new Job();

        var dateTime = LocalDateTime.ofInstant(Instant.ofEpochSecond(response.getCreatedAt()), ZoneId.systemDefault());

        job.setObjectID(response.getObjectID());
        job.setCreatedAt(Timestamp.valueOf(dateTime));
        job.setLocation(String.join(", ", response.getLocations()));
        job.setPositionName(response.getPositionName());
        job.setOrganizationName(response.getOrganization().getName());
        job.setOrganizationLogo(response.getOrganization().getLogoUrl());
        job.setUrl(response.getUrl());

        if (ArrayUtils.isEmpty(response.getOrganization().getTags())){
            job.setTags(Collections.emptyList());
        }else {
            job.setTags( Arrays.stream(response.getOrganization().getTags()).toList());
        }


        if (ArrayUtils.isEmpty(response.getHighlightResult().getJobFunctions())){
            job.setJobFunction("NOT_FOUND");
        }else {
            job.setJobFunction( Arrays.stream(response.getHighlightResult().getJobFunctions()).map(HighlightResultResponse.Item::getValue).collect(Collectors.joining(", ")));

        }
        job.setDescription("NOT_FOUND");

        return job;
    }


}
