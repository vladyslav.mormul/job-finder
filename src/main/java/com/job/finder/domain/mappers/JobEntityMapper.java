package com.job.finder.domain.mappers;

import com.fasterxml.jackson.databind.json.JsonMapper;
import com.job.finder.db.entities.JobEntity;
import com.job.finder.domain.models.Job;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collections;

@Component
public class JobEntityMapper {
    private final JsonMapper jsonMapper = new JsonMapper();

    public Job toJob(JobEntity jobEntity) {
        var job = new Job();

        job.setObjectID(jobEntity.getObjectID());
        job.setCreatedAt(jobEntity.getCreatedAt());
        job.setLocation(jobEntity.getLocation());
        job.setPositionName(jobEntity.getPositionName());
        job.setOrganizationName(jobEntity.getOrganizationName());
        job.setOrganizationLogo(jobEntity.getOrganizationLogo());
        job.setUrl(jobEntity.getUrl());
        job.setJobFunction(jobEntity.getJobFunction());
        job.setDescription(jobEntity.getDescription());
        job.setJobFunction(jobEntity.getJobFunction());

        try {
            job.setTags( Arrays.stream(jsonMapper.readValue(jobEntity.getTags(), String[].class)).toList());
        }catch (Exception e){
            job.setTags(Collections.emptyList());
        }

        return job;
    }

    public JobEntity toJobEntity(Job job) {
        var jobEntity = new JobEntity();

        jobEntity.setObjectID(job.getObjectID());
        jobEntity.setCreatedAt(job.getCreatedAt());
        jobEntity.setLocation(job.getLocation());
        jobEntity.setPositionName(job.getPositionName());
        jobEntity.setOrganizationName(job.getOrganizationName());
        jobEntity.setOrganizationLogo(job.getOrganizationLogo());
        jobEntity.setUrl(job.getUrl());
        jobEntity.setJobFunction(job.getJobFunction());
        jobEntity.setDescription(job.getDescription());
        jobEntity.setJobFunction(job.getJobFunction());

        try {
            jobEntity.setTags(jsonMapper.writeValueAsString(job.getTags()));
        }catch (Exception e){
            jobEntity.setTags("[]");
        }

        return jobEntity;
    }
}
