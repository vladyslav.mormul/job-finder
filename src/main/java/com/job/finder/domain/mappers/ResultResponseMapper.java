package com.job.finder.domain.mappers;

import com.job.finder.domain.models.ContentContainer;
import com.job.finder.domain.models.Job;
import com.job.finder.net.responses.ResultResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
@RequiredArgsConstructor
public class ResultResponseMapper {
    private final JobMapper jobMapper;

    public ContentContainer<Job> toContentContainer(ResultResponse response) {
        var hit = response.getResults()[0];

        var content = Arrays.stream(hit.getJobs()).map(jobMapper::toJob).toList();

        return ContentContainer.<Job>builder()
                .content(content)
                .page(hit.getPage())
                .size(hit.getSize())
                .totalPages(hit.getTotalPages())
                .totalSize(hit.getTotalElements())
                .build();
    }
}
