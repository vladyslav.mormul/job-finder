package com.job.finder.domain.mappers;

import com.job.finder.domain.filters.JobFilter;
import com.job.finder.net.search.HitSearch;
import org.springframework.stereotype.Component;
import org.thymeleaf.util.ArrayUtils;

import java.util.Arrays;
import java.util.Collections;

@Component
public class JobFilterMapper {

    public HitSearch toHitSearch(JobFilter filter){
        HitSearch hitSearch = new HitSearch();

        hitSearch.setPage(filter.getPage());
        hitSearch.setSize(filter.getSize());

        if (ArrayUtils.isEmpty(filter.getJobFunctions())) {
            hitSearch.setJobFunctions(Collections.emptyList());
        }else {
            hitSearch.setJobFunctions(Arrays.stream(filter.getJobFunctions()).toList());
        }

        if (ArrayUtils.isEmpty(filter.getObjectIDs())) {
            hitSearch.setObjectIDs(Collections.emptyList());
        }else {
            hitSearch.setObjectIDs(Arrays.stream(filter.getObjectIDs()).toList());
        }

        return hitSearch;
    }
}
