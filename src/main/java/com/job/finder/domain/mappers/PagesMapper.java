package com.job.finder.domain.mappers;

import com.job.finder.db.entities.JobEntity;
import com.job.finder.domain.models.ContentContainer;
import com.job.finder.domain.models.Job;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class PagesMapper {
    private final JobEntityMapper jobEntityMapper;


    public ContentContainer<Job> toContentContainer(Page<JobEntity> pages) {
        long size = pages.getSize();
        long page = pages.getNumberOfElements();
        long totalSize = pages.getTotalElements();
        long totalPages = pages.getTotalPages();

        return new ContentContainer<>(
                size,
                page,
                totalSize,
                totalPages,
                pages.getContent().stream().map(jobEntityMapper::toJob).toList()
        );
    }
}
