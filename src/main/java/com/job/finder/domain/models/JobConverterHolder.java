package com.job.finder.domain.models;

import com.job.finder.domain.converters.JobConverter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
public class JobConverterHolder {

    private final Map<ConverterType, JobConverter> converters;

    public JobConverterHolder(List<JobConverter> converters) {
        this.converters = converters.stream().collect(
                java.util.stream.Collectors.toMap(JobConverter::getType, converter -> converter));
    }

    public Optional<JobConverter> getConverter(ConverterType type) {
        return Optional.ofNullable(converters.get(type));
    }
}
