package com.job.finder.domain.models;

public enum ConverterType {
    CSV, SQL;

    public String getExtension() {
        return this.name().toLowerCase();
    }
}
