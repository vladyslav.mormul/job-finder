package com.job.finder.domain.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContentContainer<T> {
    private Long size;
    private Long page;
    private Long totalSize;
    private Long totalPages;
    private List<T> content;
}
