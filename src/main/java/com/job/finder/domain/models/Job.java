package com.job.finder.domain.models;

import com.job.finder.domain.DateTieUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Job {

    private String objectID;
    private Timestamp createdAt;
    private String location;
    private String positionName;
    private String organizationName;
    private String organizationLogo;
    private String url;
    private List<String> tags;
    private String description;
    private String jobFunction;

    public  String toFormatCreatedAt() {
        return DateTieUtils.FORMAT.format(createdAt.toLocalDateTime());
    }
}
