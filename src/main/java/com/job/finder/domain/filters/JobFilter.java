package com.job.finder.domain.filters;

import lombok.*;

import java.util.Collection;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JobFilter {
    private Long page;
    private Long size;
    private String[] jobFunctions;
    private String[] objectIDs;
    private boolean archive;
}
