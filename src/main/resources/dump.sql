drop database IF exists job_db;
CREATE DATABASE job_db;
use job_db;

CREATE TABLE job (
    objectID VARCHAR(255) PRIMARY KEY,
    createdAt TIMESTAMP,
    location VARCHAR(255),
    positionName VARCHAR(255),
    organizationName VARCHAR(255),
    organizationLogo VARCHAR(255),
    url VARCHAR(255),
    description TEXT,
    jobFunction VARCHAR(255),
    tags JSON
);
