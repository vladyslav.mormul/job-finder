# Job Finder

Job Finder is a Java-based web application that allows users to search and apply for jobs. It uses Spring Boot for the backend, SQL for data storage, and Maven for project management.

## Features

- Search for jobs based on various filters
- Apply for jobs
- Save and archive jobs
- Delete jobs

For installation instructions, please refer to the `INSTALL.md` file.