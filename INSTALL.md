# Installation Guide

This guide will help you set up and run the Job Finder project on your local machine.

## Prerequisites
- Java 17 or higher
- MySQL 8.0 or higher
- Maven 3.6.0 or higher
- IntelliJ IDEA 2023.2.4 or any other IDE that supports Spring Boot

## Steps

1. Clone the repository to your local machine.
2. Open the project in your IDE.
3. Create a new database in MySQL. You can use the provided `dump.sql` file to create and populate the database. Run the following command in your MySQL command line interface, replacing `your_database_name` with the name of your database:
    `src/main/resources/dump.sql`
4. Update the `src/main/resources/application.properties` file with your MySQL credentials and database details:
    - `spring.datasource.url`: Your database URL (default is `jdbc:mysql://localhost:3306/your_database_name`)
    - `spring.datasource.username`: Your MySQL username (default is `root`)
    - `spring.datasource.password`: Your MySQL password
5. Run the command `mvn clean install` in the project root directory to build the project.
6. Run the main class `com.job.finder.JobFinderApplication` to start the application.

The application should now be running and accessible on `http://localhost:8080`.